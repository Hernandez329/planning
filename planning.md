## Feature 1
* [x] Fork and clone the starter project from Project Alpha  
* [x] Create a new virtual environment in the repository directory for the project
  * [x] python -m venv .venv 
* [x] Activate the virtual environment
  * [x] source .venv/bin/activate 
* [x] Upgrade pip
  * [x] pip install --upgrade pip 
* [x] Install django
  * [x] pip install django 
* [x] Install black
  * [x] pip install black
* [x] Install flake8
  * [x] pip install flake8 
* [ ] Install djlint
  * [ ] pip install djlint
* [ ] Install djhtml
  * [ ] pip install djhtml 
* [ ] Deactivate your virtual environment
* [ ] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
  * [x] pip freeze > requirements.txt 

###

## Feature 2
* [x] Create a Django project named tracker so that the manage.py file is in the top directory
* [x] Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list
* [x] Run the migrations
* [x] Create a super user


 
## Feature 5

* [x] Create a view that will get all of the instances of the **Project** model and puts them in the context for the template
* [ ] Register that view in the **projects app** for the path "" and the name **"list_projects"** in a new file named projects/urls.py
* [ ] Include the URL patterns from the **projects** app in the tracker project with the prefix **"projects/"**
* [ ] Create a template for the list view that complies with the following specifications